drop table if exists card;
drop table if exists mobile_user;
drop table if exists final_user;
drop table if exists organization;
drop table if exists user_organization;
drop table if exists m_transaction;


create table card(
    id integer primary key autoincrement,
    code varchar(64)
);

create table mobile_user(
    id integer primary key autoincrement,
    name varchar(64),
    surname varchar(64) DEFAULT Null,
    image_url varchar(64) DEFAULT Null
);

create table final_user(
    id integer primary key autoincrement,
    name varchar(64),
    surname varchar(64) DEFAULT Null,
    image_url varchar(64) DEFAULT Null,
    id_card integer,
    FOREIGN KEY(id_card) REFERENCES card(id) ON DELETE CASCADE
);

create table organization(
    id integer primary key autoincrement,
    name varchar(64),
    image_url varchar(64) DEFAULT Null,
    payment_info varchar(64)
);

create table user_organization(
    id integer primary key autoincrement,
    id_organization integer,
    id_user integer,
    FOREIGN KEY(id_organization) REFERENCES organization(id) ON DELETE CASCADE,
    FOREIGN KEY(id_user) REFERENCES final_user(id) ON DELETE CASCADE
);

create table m_transaction(
    id integer primary key autoincrement,
    id_organization integer,
    id_final_user integer,
    id_mobile_user integer,
    organization_sum float,
    user_sum float,
    FOREIGN KEY(id_organization) REFERENCES organization(id) ON DELETE CASCADE,
    FOREIGN KEY(id_final_user) REFERENCES final_user(id) ON DELETE CASCADE,
    FOREIGN KEY(id_mobile_user) REFERENCES mobile_user(id) ON DELETE CASCADE
);

INSERT INTO `card` (`id`, `code`) VALUES (NULL, '111'), (NULL, '222');
INSERT INTO `final_user` (`id`, `name`, `surname`, `image_url`, `id_card`) VALUES (NULL, 'Mario', 'Rossi', NULL, '1');
INSERT INTO `mobile_user` (`id`, `name`, `surname`, `image_url`) VALUES (NULL, 'Federico', 'Bianchi', NULL);
INSERT INTO `organization` (`id`, `name`, `image_url`, `payment_info`) VALUES (NULL, 'San Giuseppe', 'img/onlus.jpg', '1111');
INSERT INTO `user_organization` (`id`, `id_organization`, `id_user`) VALUES (NULL, '1', '1');
INSERT INTO `m_transaction` (`id`, `id_organization`, `id_final_user`, `id_mobile_user`, `organization_sum`, `user_sum`) VALUES (NULL, '1', '1', '1', '2.0', '3.0');