import sqlite3
from flask import Flask, g, request, jsonify
from . import app


@app.route('/confirm', methods=['GET', 'POST'])
def confirm():
    id_user = '0'
    id_app = '0'
    sum_user = float(0)
    sum_org = float(0)
    if request.method == 'POST':
        if request.form['id_user']:
            id_user = request.form['id_user']
        if request.form['id_app']:
            id_app = request.form['id_app']
        if request.form['sum_user']:
            sum_user = request.form['sum_user']
        if request.form['sum_org']:
            sum_org = request.form['sum_org']
    else:
        if request.args.get('id_user'):
            id_user = request.args.get('id_user')
        if request.args.get('id_app'):
            id_app = request.args.get('id_app')
        if request.args.get('sum_user'):
            sum_user = request.args.get('sum_user')
        if request.args.get('sum_org'):
            sum_org = request.args.get('sum_org')
    id_org = '0'
    # extract id_org
    file_entry = g.db.execute('SELECT id_organization FROM user_organization WHERE id_user=?', [id_user])
    for row in file_entry:
        id_org = row[0]
    g.db.commit()
    if id_user == '0' or id_app == '0' or id_org == '0':
        return jsonify(0)
    g.db.execute('INSERT INTO `m_transaction` (`id`, `id_organization`, `id_final_user`, `id_mobile_user`, `organization_sum`, `user_sum`) VALUES (NULL, ?, ?, ?, ?, ?);',
                 [id_org, id_user, id_app, sum_org, sum_user])
    g.db.commit()
    return jsonify(1)



@app.route('/get_info', methods=['GET', 'POST'])
def get_info():
    card_code = 0
    if request.method == 'POST':
        if request.form['cc']:
            card_code = request.form['cc']
    else:
        if request.args.get('cc'):
            card_code = request.args.get('cc')
    id_org = 0
    id_user = 0
    id_card = 0
    name_org = ''
    name_user = ''
    image_org = ''
    image_user = ''
    payment_info = ''
    print(card_code)
    # extract card info
    file_entry = g.db.execute('SELECT id FROM card WHERE code=?', [str(card_code)])
    for row in file_entry:
        id_card = row[0]
    g.db.commit()
    if id_card == 0:
        return jsonify(0)
    else:
        print(id_card)

    # extract user info
    file_entry = g.db.execute('SELECT id,name, image_url FROM final_user WHERE id_card=?', [id_card])
    for row in file_entry:
        id_user = row[0]
        name_user = row[1]
        image_user = row[2]
    g.db.commit()

    # extract id_org
    file_entry = g.db.execute('SELECT id_organization FROM user_organization WHERE id_user=?', [id_user])
    for row in file_entry:
        id_org = row[0]
    g.db.commit()

    # extract organization_info
    file_entry = g.db.execute('SELECT name, image_url, payment_info FROM organization WHERE id=?', [id_org])
    for row in file_entry:
        name_org = row[0]
        image_org = row[1]
        payment_info = row[2]
    g.db.commit()
    a = {
        'card_id': id_card,
        'org_id': id_org,
        'org_name': name_org,
        'org_img': image_org,
        'org_pay': payment_info,
        'id_user': id_user,
        'user_name': name_user,
        'user_img': image_user
    }
    return jsonify(a)
