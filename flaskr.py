# Hackathon Cashless - Nexi
# Milano digital week
# social: #cashlesshack
#
# Authors: Masciadri, Botta, Foddegit push -u origin master

#.-------------------------


# all the imports
import sqlite3
from flask import Flask, g
from contextlib import closing
import server

server.app = Flask(__name__)
server.app.config.from_object('config')
gv = ''
from server import api,routes


def init_db():
    with closing(connect_db()) as db:
        with server.app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


@server.app.before_request
def before_request():
    g.db = connect_db()


@server.app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


def connect_db():
    return sqlite3.connect(server.app.config['DATABASE'])



if __name__ == '__main__':
    #init_db()  # uncomment to initialize the database
    server.app.run()






